[![Rust version]( https://img.shields.io/badge/rust-1.20-blue.svg)]()
[![Latest version](https://img.shields.io/crates/v/bindiff.svg)](https://crates.io/crates/bindiff)
[![All downloads](https://img.shields.io/crates/d/bindiff.svg)](https://crates.io/crates/bindiff)
[![Downloads of latest version](https://img.shields.io/crates/dv/bindiff.svg)](https://crates.io/crates/bindiff)

Binary (executable) file diff tool written in Rust

## Installation

```bash
$ cargo install bindiff
```

## Usage

```bash
$ bindiff /bin/ls /bin/cat
...
```
