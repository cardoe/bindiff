// Copyright 2017 Doug Goldstein <cardoe@cardoe.com>
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use failure::{err_msg, Error, ResultExt};
use goblin::elf;
use prettytable::Table;
use std::fs::File;
use std::io::Read;
use std::path::Path;

pub struct ElfFile<'a> {
    elf: elf::Elf<'a>,
    buf: &'a [u8],
}

pub fn load<P: AsRef<Path>>(path: P, buf: &mut Vec<u8>) -> Result<ElfFile, Error> {
    let mut fd = File::open(&path)?;
    fd.read_to_end(buf)?;
    let elf = elf::Elf::parse(buf)
        .with_context(|e| format_err!("{} as not an ELF: {}", path.as_ref().display(), e))?;
    Ok(ElfFile {
        elf,
        buf: buf.as_slice(),
    })
}

macro_rules! show_diff {
    ($name:expr, $field:ident, $file1:expr, $file2:expr) => { {
        println!("file1 {}: {:?}", $name, $file1.$field);
        println!("file2 {}: {:?}", $name, $file2.$field);
        Err(err_msg("files differ"))
    } }
}

macro_rules! field_diff {
    ($t:expr, $file1:expr, $file2:expr, $field:ident) => { {
        if $file1.$field != $file2.$field {
            $t.add_row(row![stringify!($field), $file1.$field, $file2.$field]);
        }
    } }
}


fn show_header(file1: elf::Header, file2: elf::Header) {
    // create a table
    let mut table = Table::new();

    // add header
    table.set_titles(row!["Field", "file1", "file2"]);

    // add fields
    //table.add_row(row!["e_ident", file1.e_ident, file2.e_ident]);
    field_diff!(table, file1, file2, e_type);
    field_diff!(table, file1, file2, e_machine);
    field_diff!(table, file1, file2, e_version);
    field_diff!(table, file1, file2, e_entry);
    field_diff!(table, file1, file2, e_phoff);
    field_diff!(table, file1, file2, e_shoff);
    field_diff!(table, file1, file2, e_flags);
    field_diff!(table, file1, file2, e_ehsize);
    field_diff!(table, file1, file2, e_phentsize);
    field_diff!(table, file1, file2, e_phnum);
    field_diff!(table, file1, file2, e_shentsize);
    field_diff!(table, file1, file2, e_shnum);
    field_diff!(table, file1, file2, e_shstrndx);

    table.printstd();
}

fn show_program_header(file1: &elf::ProgramHeader, file2: &elf::ProgramHeader) {
    // create a table
    let mut table = Table::new();

    // add header
    table.set_titles(row!["Field", "file1", "file2"]);

    // add fields
    field_diff!(table, file1, file2, p_type);
    field_diff!(table, file1, file2, p_flags);
    field_diff!(table, file1, file2, p_offset);
    field_diff!(table, file1, file2, p_vaddr);
    field_diff!(table, file1, file2, p_paddr);
    field_diff!(table, file1, file2, p_filesz);
    field_diff!(table, file1, file2, p_memsz);
    field_diff!(table, file1, file2, p_align);

    table.printstd();
}

pub fn diff(file1: ElfFile, file2: ElfFile) -> Result<(), Error> {
    let mut ret = Ok(());

    // diff the ELF header
    if file1.elf.header != file2.elf.header {
        println!("ELF header");
        show_header(file1.elf.header, file2.elf.header);
        ret = Err(err_msg("files differ"));
    }

    if file1.elf.program_headers != file2.elf.program_headers {
        // if file1 has more program headers then we want to extend
        // it with empty program headers for the diff behavior otherwise
        // the reverse or they're the same size so there's no issue
        {
            // number of program headers we have
            let f1_len = file1.elf.program_headers.len();
            let f2_len = file2.elf.program_headers.len();
            // endless iterator of empty program headers
            let empty = vec![elf::ProgramHeader::new()];
            let endless = empty.iter().cycle();

            if f1_len > f2_len {
                let _: Vec<()> = file1.elf.program_headers
                    .iter()
                    .zip(file2.elf.program_headers
                         .iter()
                         .chain(endless))
                    .filter_map(|(f1, f2)| {
                        if f1 != f2 {
                            Some((f1, f2))
                        } else {
                            None
                        }
                    })
                .map(|(f1, f2)| show_program_header(f1, f2))
                    .collect();
            } else if f1_len < f2_len {
                let _: Vec<()>  = file1.elf.program_headers
                    .iter()
                    .chain(endless)
                    .zip(file2.elf.program_headers
                         .iter())
                    .map(|(f1, f2)| show_program_header(f1, f2))
                    .collect();
            } else {
                let _: Vec<()> = file1.elf.program_headers
                    .iter()
                    .zip(file2.elf.program_headers
                         .iter())
                    .map(|(f1, f2)| show_program_header(f1, f2))
                    .collect();
            };
        }
        ret = Err(err_msg("files differ"));
    }

    if file1.elf.section_headers != file2.elf.section_headers {
        ret = show_diff!("section headers", section_headers, file1.elf, file2.elf);
    }

    ret
}
