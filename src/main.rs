// Copyright 2017 Doug Goldstein <cardoe@cardoe.com>
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#[macro_use]
extern crate failure;
extern crate goblin;
#[macro_use]
extern crate prettytable;
extern crate structopt;
#[macro_use]
extern crate structopt_derive;

mod elf;

use failure::Error;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "elfdiff", about = "A tool to provide the differences between two ELFs")]
struct Opt {
    /// first file to compare
    #[structopt(help = "Input file")]
    file1: String,

    /// second file to compare
    #[structopt(help = "Input file")]
    file2: String,
}

fn run() -> Result<(), Error> {
    let opt = Opt::from_args();

    let mut file1_buf = Vec::new();
    let mut file2_buf = Vec::new();
    let file1 = elf::load(opt.file1, &mut file1_buf)?;
    let file2 = elf::load(opt.file2, &mut file2_buf)?;

    elf::diff(file1, file2)
}

fn main() {
    match run() {
        Ok(_) => {}
        Err(e) => {
            eprintln!("{:#}", e);
            ::std::process::exit(1);
        }
    }
}
